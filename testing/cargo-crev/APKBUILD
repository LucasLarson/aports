# Contributor: lauren n. liberda <lauren@selfisekai.rocks>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=cargo-crev
pkgver=0.25.5
pkgrel=2
pkgdesc="Cryptographically verifiable code review system for cargo"
url="https://github.com/crev-dev/cargo-crev"
# s390x, armhf: failing tests
arch="all !s390x !armhf"
license="MPL-2.0 OR MIT OR Apache-2.0"
makedepends="cargo cargo-auditable openssl-dev"
source="https://github.com/crev-dev/cargo-crev/archive/v$pkgver/cargo-crev-$pkgver.tar.gz
	use-index-guix-from-crates.io.patch
	cargo-update.patch
	"

export OPENSSL_NO_VENDOR=1

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release -p cargo-crev --no-default-features
}

check() {
	# Increase stack size to avoid stack overflow while compiling 'p384'.
	export RUST_MIN_STACK="4194304"

	cargo test --frozen --no-default-features
}

package() {
	install -Dm755 target/release/cargo-crev "$pkgdir"/usr/bin/cargo-crev
}

sha512sums="
621447b0f5e32ed11af3d4e985d77986e94ceb6d6e0359880e70c20120698fb685b64062824fbd0bfffd9de565a6d5894788996c74637444d7bbdb47ed8e3807  cargo-crev-0.25.5.tar.gz
84123bf4c1f1dfcb4dc1e5b31f5b5351a21cf24a56a8b0e07bffce00b5c0170f373dbb1d3d293beff4cc3f4873e831f9967da872f7408cf491f8926b1cc717c0  use-index-guix-from-crates.io.patch
7168d7d5c2fa789212ec660662c2e36bf06aeff8020a122ba7889c150ec583be066ad5c9a81f31e66c8be61ad1c8e646c06b8d878346f6cca1e02b8e544e067a  cargo-update.patch
"
